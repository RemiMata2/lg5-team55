package ee.ut.math.tvt.salessystem.dataobjects;
import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

@Entity
@Table(name = "HistoryTransaction")
public class HistoryTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Date")
    private  String date;

    @Column(name = "Time")
    private  String time;

    @Column(name = "Total")
    private double total;


    public HistoryTransaction() {

    }
    public HistoryTransaction(String date, String time,double total) {
        this.date = date;
        this.time = time;
        this.total = total;
    }



    public Long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getData_date() {
        SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date data_date = null;
        try {
            data_date = formatter.parse(date + " "+time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return data_date;
    }
}
