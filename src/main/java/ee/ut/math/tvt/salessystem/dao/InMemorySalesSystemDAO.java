package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryTransaction;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO{
    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<HistoryTransaction> historyTransactionList;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.historyTransactionList = new ArrayList<>();
    }


    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public List<HistoryTransaction> showLastTen() {
        int len = historyTransactionList.size();
        if(len>10){
            return historyTransactionList.subList(len-9,len);
        }
        else {
            return historyTransactionList;
        }
    }

    @Override
    public List<HistoryTransaction> showAll() {
        return historyTransactionList;
    }

    @Override
    public List<HistoryTransaction> showBetweenDates(Date date1, Date date2) {
        List<HistoryTransaction> betweenDatesList = new ArrayList<>();
        for(HistoryTransaction historyTransaction : historyTransactionList){
            if((date1.before(historyTransaction.getData_date()) && date2.after(historyTransaction.getData_date())) || (date1.equals(historyTransaction.getData_date())) || (date2.equals(historyTransaction.getData_date())) ){
                betweenDatesList.add(historyTransaction);
            }
        }
        return betweenDatesList;
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public List<SoldItem> findSoldItems(String date, String time) {
        List<SoldItem> soldItemList_final = new ArrayList<>();
        for (SoldItem item : soldItemList){
            if((item.getDate()==date) && (item.getTime()==time)){
                soldItemList_final.add(item);
            }
        }
        return soldItemList_final;
    }

    @Override
    public void beginTransaction() {

    }

    @Override
    public void rollbackTransaction() {

    }

    @Override
    public void commitTransaction() {

    }

    @Override
    public void commitPurshase(String date, String time) {

        try{
            List<SoldItem> soldItemList = findSoldItems(date,time);
            HistoryTransaction historyTransaction = new HistoryTransaction(date,time,-1);
            double final_total = 0;
            for(SoldItem item : soldItemList){
                StockItem item_stocked = item.getStockItem();
                final_total += item_stocked.getPrice() * item.getQuantity();
                item_stocked.setQuantity(item_stocked.getQuantity()-item.getQuantity());
            }

            historyTransaction.setTotal(final_total);
            historyTransactionList.add(historyTransaction);

        }
        catch (Exception e){
        }

    }

    public List<HistoryTransaction> getHistoryTransactionList() {
        return historyTransactionList;
    }

    public List<SoldItem> getSoldItems() {
        return soldItemList;
    }
}
