package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryTransaction;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {
    private static final Logger log = LogManager.getLogger(HibernateSalesSystemDAO.class);
    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        beginTransaction();
        try{
            List<StockItem> list_StockItem = em.createQuery("SELECT s FROM StockItem s",StockItem.class).getResultList();
            commitTransaction();
            return list_StockItem;
        }
        catch (Exception e){
            log.debug(e);
            rollbackTransaction();
            return null;
        }


    }

    @Override
    public StockItem findStockItem(long id) {
        beginTransaction();
        try{
            StockItem stockItem = em.find(StockItem.class,id);
            commitTransaction();
            return stockItem;

        }
        catch (Exception e){
            log.debug(e);
            rollbackTransaction();
            return null;
        }
    }



    @Override
    public List<HistoryTransaction> showLastTen() {
        List<HistoryTransaction> list_HistoryTransaction = em.createQuery("SELECT h FROM HistoryTransaction h").getResultList();
        int len = list_HistoryTransaction.size();
        if(len <10 ){
            return list_HistoryTransaction;
        }
        else{
            return list_HistoryTransaction.subList(len-9,len);
        }
    }

    @Override
    public List<HistoryTransaction> showAll() {
        beginTransaction();
        try {
            List<HistoryTransaction> historyTransactionList = em.createQuery("SELECT h FROM HistoryTransaction h", HistoryTransaction.class).getResultList();
            commitTransaction();
            return historyTransactionList;
        }
        catch (Exception e){
            log.debug(e);
            rollbackTransaction();
            return null;
        }
    }

    @Override
    public List<HistoryTransaction> showBetweenDates(Date date1, Date date2) {
        List<HistoryTransaction> list_HistoryTransaction = em.createQuery("SELECT h FROM HistoryTransaction h").getResultList();
        List<HistoryTransaction> betweenDatesList = new ArrayList<>();
        for(HistoryTransaction historyTransaction : list_HistoryTransaction){
            try {
                Date date = new SimpleDateFormat("dd/MM/yyyy").parse(historyTransaction.getDate());
                if((date1.before(date) && date2.after(date) || (date1.equals(date) || (date2.equals(date) )))){
                    betweenDatesList.add(historyTransaction);
                }
            }
            catch (Exception e) {
                log.debug(e);
            }
        }
        return betweenDatesList;
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        try{
            em.merge(stockItem);
            commitTransaction();
        }
        catch (Exception e){
            log.debug(e);
            rollbackTransaction();
        }
    }

    @Override
    public void saveSoldItem(SoldItem soldItem) {
        beginTransaction();
        try{
            em.merge(soldItem);
            commitTransaction();
        }
        catch (Exception e){
            log.debug(e);
            rollbackTransaction();
        }
    }

    @Override
    public List<SoldItem> findSoldItems(String date, String time){
        return em.createQuery("SELECT s FROM SoldItem s WHERE s.date = :custDate AND s.time = :custTime",SoldItem.class).setParameter("custDate", date).setParameter("custTime", time).getResultList();
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void commitPurshase(String date, String time){
        beginTransaction();
        try{
            List<SoldItem> soldItemList = findSoldItems(date,time);
            HistoryTransaction historyTransaction = new HistoryTransaction(date,time,-1);
            double final_total = 0;
            for(SoldItem item : soldItemList){
                StockItem item_stocked = item.getStockItem();
                final_total += item_stocked.getPrice() * item.getQuantity();
                item_stocked.setQuantity(item_stocked.getQuantity()-item.getQuantity());
                em.merge(item_stocked);
            }
            historyTransaction.setTotal(final_total);
            em.merge(historyTransaction);
            commitTransaction();
        }
        catch (Exception e){
            log.debug(e);
            rollbackTransaction();
        }


    }


}