package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ShoppingCart {

    //private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    private String date;
    private String time;


    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) throws Exception {
        // TODO In case such stockItem already exists increase the quantity of the existing stock
        if(item.getQuantity()<=0){
            throw new Exception("quantity negative !");
        }
        boolean added = false;
        if (items.size()>0){
            for (int i=0; i<=items.size()-1; i++){
                if(items.get(i).getStockItem().getName().equals(item.getStockItem().getName())){
                    int quantity=items.get(i).getQuantity()+item.getQuantity();
                    if (quantity<= dao.findStockItem(item.getId()).getQuantity()){
                        if (added == false) {
                            item.setQuantity(quantity);
                            items.set(i, item);
                            added = true;
                        }
                    }
                    else {
                        added=true;
                        throw new Exception("no stock");
                        //log.error("no stock");
                    }

                }
                if(added == false && i == items.size() - 1){
                    if (item.getQuantity() <= dao.findStockItem(item.getId()).getQuantity()) {
                        added=true;
                        items.add(item);
                    } else {
                        added=true;
                        throw new Exception("no stock");
                        //log.error("no stock");
                    }
                }

            }
        }
        // TODO verify that warehouse items' quantity remains at least zero or throw an exception

        else {
            if (item.getQuantity() <= dao.findStockItem(item.getId()).getQuantity() && added == false) {
                added=true;
                items.add(item);
            } else {
                added=true;
                throw new Exception("no stock");
                //log.error("no stock");
            }
        }
        //log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        dao.beginTransaction();
        dao.commitTransaction();
        // TODO decrease quantities of the warehouse stock

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        try {
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
            }
            dao.commitPurshase(this.date, this.time);
            items.clear();
        } catch (Exception e) {
            throw e;
        }
    }

    public void refresh_date(){
        Date data_date = new Date();
        String date_pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(date_pattern, new Locale("en", "UK"));
        this.date = simpleDateFormat.format(data_date);

        String time_pattern = "HH:mm:ss";
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat(time_pattern, new Locale("en", "UK"));
        this.time = simpleTimeFormat.format(data_date);
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }
}
