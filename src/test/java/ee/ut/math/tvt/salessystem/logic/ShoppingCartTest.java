
package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryTransaction;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import junit.framework.TestCase;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.mock;


public class ShoppingCartTest extends TestCase {


    private StockItem stockItem;
    private SoldItem soldItem;


    @Before
    public void setUp() throws Exception {
        stockItem = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5);
        soldItem = new SoldItem(stockItem,1,"2021-11-16","11:00:21");
    }

    @Test()
    public void testAddingExistingItem () {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        try {
            cart.addItem(new SoldItem(stockItem,1,"2021-11-16","11:00:21"));
            cart.addItem(new SoldItem(stockItem,3,"2021-11-16","11:00:21"));
            assertEquals(new Double(cart.getAll().get(0).getQuantity()),4.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test()
    public void testAddingNewItem () {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        try {
            cart.addItem(soldItem);
            assertEquals(soldItem, cart.getAll().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test()
    public void testAddingItemWithNegativeQuantity() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        try {
            cart.addItem(new SoldItem(stockItem,-1,"2021-11-16","11:00:21"));
        } catch (Exception e) {
            assertEquals(e.getMessage(),"quantity negative !");
        }
    }

    @Test()
    public void testAddingItemWithQuantityTooLarge(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        try {
            cart.addItem(new SoldItem(stockItem,6,"2021-11-16","11:00:21"));
        } catch (Exception e) {
            assertEquals(e.getMessage(),"no stock");
        }
    }

    @Test()
    public void testAddingItemWithQuantitySumTooLarge(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        try {
            cart.addItem(new SoldItem(stockItem,1,"2021-11-16","11:00:21"));
            cart.addItem(new SoldItem(stockItem,5,"2021-11-16","11:00:21"));
        } catch (Exception e) {
            assertEquals(e.getMessage(),"no stock");
        }
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        cart.refresh_date();
        StockItem test_stockItem = dao.findStockItems().get(0);
        int initial_quantity = test_stockItem.getQuantity();
        try {
            cart.addItem(new SoldItem(test_stockItem,1,cart.getDate(),cart.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        cart.submitCurrentPurchase();
        int final_quantity = dao.findStockItem(stockItem.getId()).getQuantity();
        assertEquals(initial_quantity-1, final_quantity);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction(){
        SalesSystemDAO daoMock = mock(InMemorySalesSystemDAO.class);
        ShoppingCart cart = new ShoppingCart(daoMock);
        try {
            cart.addItem(soldItem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cart.submitCurrentPurchase();

        InOrder orderVerifier = Mockito.inOrder(daoMock);

        orderVerifier.verify(daoMock).beginTransaction();
        orderVerifier.verify(daoMock).commitTransaction();
    }



    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        try {
            cart.addItem(new SoldItem(stockItem,6,"2021-11-16","11:00:21"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Date data_date = new Date();
        String date_pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(date_pattern, new Locale("en", "UK"));
        String testDate = simpleDateFormat.format(data_date);
        String time_pattern = "HH:mm:ss";
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat(time_pattern, new Locale("en", "UK"));
        String testTime = simpleTimeFormat.format(data_date);

        cart.refresh_date();
        cart.submitCurrentPurchase();

        List<HistoryTransaction> historyTransactionList = ((InMemorySalesSystemDAO) dao).getHistoryTransactionList();
        String purchaseDate = historyTransactionList.get(0).getDate();
        String purchaseTime = historyTransactionList.get(0).getTime();

        assertEquals( testTime, purchaseTime);
        assertEquals( testDate, purchaseDate);
    }

    @Test
    public void testCancellingOrder(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        cart.refresh_date();
        SoldItem item1=new SoldItem(dao.findStockItems().get(0),1,cart.getDate(),cart.getTime());
        SoldItem item2 = new SoldItem(dao.findStockItems().get(1),1,cart.getDate(),cart.getTime());
        try {
            cart.addItem(item1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cart.cancelCurrentPurchase();

        try {
            cart.addItem(item2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cart.submitCurrentPurchase();
        assertEquals(((InMemorySalesSystemDAO) dao).getSoldItems().get(0),item2);
    }


    @Test
    public void testCancellingOrderQuantitiesUnchanged(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        cart.refresh_date();
        StockItem init_stockItem = dao.findStockItems().get(0);
        int initial_quantity = init_stockItem.getQuantity();
        try {
            cart.addItem(new SoldItem(init_stockItem,1,cart.getDate(),cart.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        cart.cancelCurrentPurchase();
        StockItem final_stockItem = dao.findStockItems().get(0);
        int final_quantity = final_stockItem.getQuantity();
        assertEquals(initial_quantity, final_quantity);
    }

}

