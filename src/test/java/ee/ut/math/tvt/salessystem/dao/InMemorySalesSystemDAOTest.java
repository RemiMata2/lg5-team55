/*

package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SalesSystemDAOTest extends TestCase {

    private StockItem stockItem1;
    private StockItem stockItem2;

    @org.junit.Before
    public void setUp() throws Exception {
        stockItem1 = new StockItem(10L, "Nutella", "without Palm oil", 15.0, 12);
        stockItem2 = new StockItem(1L, "Lays chips", "Potato chips", 11.0, -6);


    }



    @org.junit.Test
    public void testAddingItemBeginsAndCommitsTransaction () {

    }

    @org.junit.Test
    public void testAddingNewItem() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        dao.saveStockItem(stockItem1);
        assertEquals(stockItem1,dao.findStockItem(stockItem1.getId()));
    }


    @Test(expected = Exception.class)
    public void testAddingItemWithNegativeQuantity() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        dao.saveStockItem(stockItem2);
    }

    @org.junit.After
    public void tearDown() throws Exception {
        super.tearDown();
        stockItem1 = null;
        stockItem2 = null;
    }
}

 */