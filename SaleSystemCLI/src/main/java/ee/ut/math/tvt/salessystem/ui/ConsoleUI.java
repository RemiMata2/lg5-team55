package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final HibernateSalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(HibernateSalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        HibernateSalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");




        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("wa\t\t add a product to the warehouse");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) throws IOException {
        String[] c = command.split(" ");
        switch (c[0]){
            case "h":
                printUsage();
                break;
            case "q":
                System.exit(0);
                break;
            case "w":
                showStock();
                break;
            case "c":
                showCart();
                break;
            case "wa":
                addProduct();
                break;
            case "p":
                cart.submitCurrentPurchase();
                break;
            case "r":
                cart.cancelCurrentPurchase();
                break;

            case "a":
                if (c.length == 3) {
                    try {
                        long idx = Long.parseLong(c[1]);
                        int amount = Integer.parseInt(c[2]);
                        StockItem item = dao.findStockItem(idx);
                        if (item != null) {
                            cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity()), cart.getDate(), cart.getTime()));
                        } else {
                            System.out.println("no stock item with id " + idx);
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                break;

            default:
                System.out.println("unknown command");
                log.info("unknown command : "+c[0]);

        }
    }

    public void addProduct(){
        // add chosen item to the warehouse
        while(true){
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the bar code :");
                long bareCode = Long.parseLong(in.readLine().trim().toLowerCase());
                System.out.println("Enter the quantity :");
                int quantity = Integer.parseInt((in.readLine().trim().toLowerCase()));
                System.out.println("Enter the name :");
                String name = in.readLine().trim().toLowerCase();
                String desc = "new product";
                System.out.println("Enter the price :");
                double price = Double.parseDouble(in.readLine().trim().toLowerCase());

                //creating stock item
                StockItem stockItem = new StockItem(bareCode, name, desc, price, quantity);
                log.debug(stockItem.toString());
                if (dao.findStockItem(bareCode)!=null){
                    stockItem = dao.findStockItem(bareCode);
                    int oldQuantity=stockItem.getQuantity();
                    stockItem.setQuantity(oldQuantity+quantity);
                }

                else {
                    dao.saveStockItem(stockItem);
                }

                //Writing database


            }
            catch (Exception e){
                System.out.println("Please enter Valid values !");
                log.error(e);
            }
        }
    }
}


// This is a change for this file ! from Rémi and Théo
// This is a conflict !!