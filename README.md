# Team <write your team name here>:
1. Antoine Berliat
2. Théo Muller
3. Rémi Mata

## Homework 1:
<https://bitbucket.org/RemiMata2/lg5-team55/wiki/Homework1> 

## Homework 2:
<https://bitbucket.org/RemiMata2/lg5-team55/wiki/Homework2>

## Homework 3:
<https://bitbucket.org/RemiMata2/lg5-team55/wiki/Homework3>

## Homework 4:
<https://bitbucket.org/RemiMata2/lg5-team55/wiki/Homework4>

## Homework 5:
<Links to the solution>

## Homework 6:
<https://bitbucket.org/RemiMata2/lg5-team55/wiki/Homework%206>

## Homework 7:
<https://bitbucket.org/RemiMata2/lg5-team55/wiki/Homework%207>

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)