package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryTransaction;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;


/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {
    private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final SalesSystemDAO dao;

    @FXML
    private DatePicker start_date;
    @FXML
    private DatePicker end_date;
    @FXML
    private TableView<HistoryTransaction> purchaseHistoryView;
    @FXML
    private TableView<SoldItem> DetailHistoryView;




    public HistoryController(SalesSystemDAO dao){
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.purchaseHistoryView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                HistoryTransaction historyTransaction = purchaseHistoryView.getSelectionModel().getSelectedItem();
                DetailHistoryView.setItems(FXCollections.observableList(dao.findSoldItems(historyTransaction.getDate(),historyTransaction.getTime())));
                DetailHistoryView.refresh();
            }
        } );
    }

    public void showAll(){
        purchaseHistoryView.setItems(FXCollections.observableList(dao.showAll()));
    }
    public void showLastTen(){
        purchaseHistoryView.setItems(FXCollections.observableList(dao.showLastTen()));
    }
    public void showBetweenDates(){
        try {
            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(start_date.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(end_date.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            if(date1.after(date2)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error Date");
                alert.setHeaderText("Error Date");
                alert.setContentText("the start date must be before the end date ");
                alert.showAndWait().ifPresent(rs -> {
                    if (rs == ButtonType.OK) {
                        log.error("Error input : Pressed OK.");
                    }
                });
            }
            else{
                purchaseHistoryView.setItems(FXCollections.observableList(dao.showBetweenDates(date1, date2)));
            }
        } catch (Exception e) {
            log.error(e);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error Date");
            alert.setHeaderText("Error Date");
            alert.setContentText("Please enter valid dates");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    log.error("Error input : Pressed OK.");
                }
            });
        }
    }

}
