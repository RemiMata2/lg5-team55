package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class StockController implements Initializable {
    private static final Logger log = LogManager.getLogger(TeamController.class);

    private final SalesSystemDAO dao;

    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;

    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;


    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        refreshStockItems();

        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    @FXML //ajout qui servira a chercher si le produit existe déjà
    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;

        }
    }

    @FXML
    public void addProductEventHandler() {
        // add chosen item to the warehouse
        long bareCode;
        int quantity;
        String name;
        String desc;
        double price;

        int oldQuantity;

        try {
            //creating stock item
            bareCode =Long.parseLong(barCodeField.getText()) ;
            quantity = Integer.parseInt(quantityField.getText());
            name = nameField.getText().toString();
            desc = "new product";
            price = Double.parseDouble(priceField.getText());
            StockItem stockItem = new StockItem(bareCode, name, desc, price, quantity);
            if (dao.findStockItem(bareCode)!=null){
                stockItem = dao.findStockItem(bareCode);
                oldQuantity=stockItem.getQuantity();
                stockItem.setQuantity(oldQuantity+quantity);
            }

            else {
                dao.saveStockItem(stockItem);
            }

            warehouseTableView.refresh();
            log.debug("Warehouse refreshed !");
        }

        catch (Exception e) {
            log.error("error input "+e);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error Input");
            alert.setHeaderText("Error Input");
            alert.setContentText("Please enter a valid value ");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    log.error("Error input : Pressed OK.");
                }
            });
        }
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
            nameField.setDisable(true);
            priceField.setDisable(true);
        }
        else{
            nameField.setDisable(false);
            priceField.setDisable(false);
            quantityField.setText("");
            nameField.setText("");
            priceField.setText("");
        }
    }

    @FXML
    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
    }
}
