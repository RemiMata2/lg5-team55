package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {
    private static final Logger log = LogManager.getLogger(TeamController.class);

    @FXML
    private Label teamname;
    @FXML
    private Label teamleader;
    @FXML
    private Label teamleaderemail;
    @FXML
    private Label teammembers;
    @FXML
    private ImageView team_picture;

    public TeamController() {

    }
    public void initialize(URL location, ResourceBundle resources) {

        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("../src/main/resources/application.properties"));
            log.info("Properties loaded");


        } catch (IOException e) {
            log.error("Properties not loaded"+e);
        }

        teamname.setText(properties.getProperty("teamname"));
        teamleader.setText(properties.getProperty("teamleader"));
        teamleaderemail.setText(properties.getProperty("teamleaderemail"));
        teammembers.setText(properties.getProperty("teammembers"));

        Image picture = new Image(properties.getProperty("picturelink"));
        team_picture.setImage(picture);


    }
}
